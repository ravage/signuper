rails_root = '/srv/sites/famarunners.com'

worker_processes 4

working_directory rails_root

listen File.join(rails_root, 'tmp', 'sockets', 'unicorn.sock'), :backlog => 1024

timeout 30

pid File.join(rails_root, 'tmp', 'pids', 'unicorn.pid')
stderr_path File.join(rails_root, 'log', 'unicorn.stderr.log')
stdout_path File.join(rails_root, 'log', 'unicorn.stdout.log')

preload_app true

GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end
