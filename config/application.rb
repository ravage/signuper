require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)
CONFIG = YAML.load_file("config/config_#{Rails.env}.yml").deep_symbolize_keys
module Signuper
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Lisbon'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.enforce_available_locales = true
    config.i18n.default_locale = :pt
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components')
    config.action_mailer.default_url_options = { host: 'famarunners.com' }

    config.to_prepare do
      Devise::RegistrationsController.layout proc { |controller|
        if user_signed_in?
          'account'
        elsif manager_signed_in?
          'manage'
        else
          'application'
        end
      }

      Devise::SessionsController.layout proc{ |controller|
        resource_name == :manager ? 'manage-auth' : nil
      }

      Devise::PasswordsController.layout proc{ |controller|
        resource_name == :manager ? 'manage-auth' : nil
      }
    end
  end
end
