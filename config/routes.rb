Signuper::Application.routes.draw do
  root 'site#index'

  devise_for :users, path: '', skip: [:registrations], path_names: {
    sign_in:      'login',
    sign_out:     'logout',
    registration: 'account',
    sign_up:      'register'
  },
  controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  as :user do
    get     '/account/cancel'   =>  'devise/registrations#cancel',  as: 'cancel_user_registration'
    post    '/account'          =>  'devise/registrations#create',  as: 'user_registration'
    get     '/account/register' =>  'devise/registrations#new',     as: 'new_user_registration'
    get     '/account/edit'     =>  'devise/registrations#edit',    as: 'edit_user_registration'
    patch   '/account'          =>  'devise/registrations#update'   
    delete  '/account'          =>  'devise/registrations#destroy'
  end

  get '/account' => 'account/dashboard#index', as: 'account_dashboard'

  namespace :account do
    resource  :profile, only: [:show, :edit, :update, :create]

    resources :event_options, path: 'events', only: [] do
      resource :registrations, only: [:create, :destroy] do
        get   'individual', on: :member
        get   'team', on: :member
      end
    end

    resources :registrations, only: [:index] do
      resources :transactions, only: [:new, :create, :destroy]
    end

    resources :transactions, only: [:index]
    resources :teams
    resources :participants, only: [:destroy]
    resource  :contacts, only: [:show, :edit, :create]
  end

  devise_for :managers, path_names: {
    sign_in:  'login',
    sign_out: 'logout'
  },
  skip: [:registrations] 

  as :manager do
    get     '/manager' =>  'devise/registrations#edit',  as: 'manager_registration'
    patch   '/manager' =>  'devise/registrations#update'   
  end

  namespace :manage do
    resources :events, except: [:destroy] do
      resources :prices, only: [:new, :create, :destroy]
      resources :registrations, only: [:new, :create]
      resource  :identification
      resources :event_options, path: 'options', except: [:show]
      resources :entries
    end

    resources :registrations, only: [:index, :show] do
      get 'approve', on: :member
      get 'revoke', on: :member
    end

    resources :users, except: [:destroy, :update]
    resources :transactions, only: [:update]
    resources :teams, only: [:index, :edit, :update, :destroy] 
    resources :categories
  end

  resources :events, only: [:show]
  resource :contacts, only: [:show, :edit, :create]
end
