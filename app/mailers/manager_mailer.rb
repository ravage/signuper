class ManagerMailer < ActionMailer::Base
  default from: CONFIG[:email][:from]

  def registration(registration_id)
    @registration = Registration.find(registration_id)
    type = @registration.team_id == Team::INDIVIDUAL ? 'Individual' : 'Equipa'

    mail(
      to: CONFIG[:email][:manager],
      subject: "FamaRunners: Nova Inscrição #{type}"
    )
  end

  def transaction(registration_id)
    @registration = Registration.find(registration_id)

    mail(
      to: CONFIG[:email][:manager],
      subject: 'FamaRunners: Novo Pagamento'
    )
  end
end
