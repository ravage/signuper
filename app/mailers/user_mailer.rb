class UserMailer < ActionMailer::Base
  default from: CONFIG[:email][:from]

  def contact(message)
    @message = message

    mail(
      to: CONFIG[:email][:from],
      subject: "[FamaRunners:Contacto]: #{@message.subject}",
      reply_to: message.from)
  end

  def identification(participant_id)
    @participant = Participant.find(participant_id)

    mail(to: @participant.user.email, subject: 'Dorsal e Escalão Atribuído')
  end

  def transaction(transaction_id)
    @transaction = Transaction.find(transaction_id)

    mail(to: @transaction.user.email,
         subject: "Pagamento ##{@transaction.id} #{t(@transaction.state)}") 
  end

  def registration(registration_id)
    @registration = Registration.find(registration_id) 
    @event = @registration.event

    mail(to: @registration.user.email,
         subject: "Inscrição #{@event.name}")
  end

  def managed_registration(registration_id, participant_id)
    @registration = Registration.find(registration_id)
    @participant = Participant.find(participant_id)
    @event = @registration.event

    mail(to: @participant.user.email,
         subject: "Inscrição #{@event.name}")
  end
end
