class ApplicationController < ActionController::Base
  # before_filter :check_profile
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

  def after_sign_in_path_for(resource)
    if resource.is_a?(User)
      if resource.profile
        stored_location_for(resource) || account_dashboard_path
      else
        account_profile_path
      end
    elsif resource.is_a?(Manager)
      manager_registration_path
    else
      super
    end
  end

  def after_confirmation_path_for(resource_name, resource)
    if resource.is_a?(User)
      if resource.profile.nil?
        account_profile_path
      else
        account_dashboard_path
      end
    else
      super
    end
  end

  def check_profile
    if user_signed_in?
      if params[:controller] != 'account/profiles'
        redirect_to account_profile_path and return unless current_user.profile.try(:persisted?)
      end
    end
  end
end
