class ContactsController < ApplicationController
  respond_to :html

  def show
    email = user_signed_in? ? current_user.email : ''
    @message = Contact.new(from: email)
  end

  def create
    @message = Contact.new(params[:contact])

    if @message.save
      flash[:success] = 'Mensagem enviada com sucesso!'
    else
      flash[:alert] = 'Erro ao enviar mensagem. Por favor, verifica se todos os campos estão corretos!'
    end

    respond_with @message
  end
end
