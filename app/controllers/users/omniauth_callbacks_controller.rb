class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    auth = request.env['omniauth.auth']
    @user = User.find_for_facebook_oauth(auth, current_user)
    
    session[:first_name] = auth.info.first_name
    session[:last_name] = auth.info.last_name

    if @user && @user.persisted?
      sign_in_and_redirect @user, :event => :authentication
      set_flash_message(:notice, :success, :kind => 'Facebook') if is_navigational_format?
    elsif @user.nil?
      flash[:alert] = 'O email utilizado para o registo já se encontra no sistema. Talvez já possuas uma conta? Tenta autenticar-te ou recuperar a password.'
      redirect_to new_user_session_path
    else
      session['devise.facebook_data'] = auth
      redirect_to new_user_registration_url
    end
  end
end
