class Manage::CategoriesController < Manage::BaseController
  respond_to :html

  def index
    @categories = Category.all
  end

  def show
    @category = Category.new
  end

  def edit
    @action = 'Editar'
    @category = Category.find(params[:id])
  end

  def new
    @action = 'Adicionar'
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with @category, location: [:manage, :categories]
  end

  def update
    @category = Category.find(params[:id]) 

    if @category.update_attributes(category_params)
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with @category, location: [:manage, :categories]
  end

  def destroy
    @category = Category.find(params[:id])

    @category.destroy

    respond_with @category, location: [:manage, :categories]
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
