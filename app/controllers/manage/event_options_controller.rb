class Manage::EventOptionsController < Manage::BaseController

  respond_to :html

  def index
    @event = Event.find(params[:event_id])
  end

  def new
    @event = Event.find(params[:event_id])
    @option = @event.options.build
  end

  def edit
    @event = Event.find(params[:event_id])
    @option = @event.options.find(params[:id])
  end

  def create
    @event = Event.find(params[:event_id])
    @option = @event.options.build(option_params)

    if @option.save
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with(@option, location: [:manage, @event, :event_options])
  end

  def update
    @event = Event.find(params[:event_id])
    @option = @event.options.find(params[:id])

    if @option.update_attributes(option_params)
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end 

    respond_with(@option, location: [:manage, @event, :event_options])
  end

  private

  def option_params
    params.require(:event_option).permit(
      :name,
      :max_participants,
      :registration_limit,
      :begins_at,
      :ends_at
    )
  end
end
