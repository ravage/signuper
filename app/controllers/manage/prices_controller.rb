class Manage::PricesController < Manage::BaseController
  respond_to :html

  def new
    @event = Event.find(params[:event_id])
    @price = @event.prices.build
  end
  
  def create
    @event = Event.find(params[:event_id])
    @price = @event.prices.build(price_params)

    if @price.save
      flash[:success] = 'Operação realizada com sucesso'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação'
    end

    respond_with @price, location: [:new, :manage, @event, :price]
  end

  def destroy
    @price = Price.where(event_id: params[:event_id], id: params[:id]).take!

    if Time.zone.now.to_date > @price.date
      flash[:alert] = 'A data atual ultrapassa a data de inscrição!'
    else 
      @price.destroy
    end

    respond_with @price, location: [:new, :manage, @price.event, :price]
  end

  private

  def price_params
    params.require(:price).permit(:value, :date)
  end
end
