class Manage::IdentificationsController < Manage::BaseController
  def show
    @event = Event.find(params[:event_id])
    
    @categories = Category.all
    @participants = Participant.includes(user: :profile).
      includes(registration: :team).
      joins(:registration).
      where(event: @event, registrations: { state: Registration::VERIFIED }).
      order('profiles.first_name')

    @done, @todo = @participants.partition do |p| 
      p.identification.present? && p.category_id.present?
    end
  end

  def create
    event = Event.find(params[:event_id])
    participant = Participant.where(event: event).
      find(params[:identification][:participant_id])

    if participant.update_attributes(participant_params)
      UserMailer.delay_for(30.seconds).identification(participant.id)
    else
      flash[:alert] = 'Número de dorsal já atribuído'
    end

    redirect_to [:manage, event, :identification]
  end

  private

  def participant_params
    params.require(:identification).permit(:identification, :category_id)
  end
end
