class Manage::TeamsController < Manage::BaseController
  respond_to :html

  def index
    @teams = Team.where.not(id: Team::INDIVIDUAL).
      includes(:registrations, user: :profile).
      order(:name)
  end

  def edit
    @team = Team.find(params[:id])
  end

  def update
    @team = Team.find(params[:id])

    if @team.update_attributes(team_params)
      flash[:success] = 'Operação realizada com sucesso'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação'
    end

    respond_with @team, location: [:manage, :teams]
  end

  def destroy
    @team = Team.without_participations.find(params[:id])

    @team.destroy

    respond_with @team, location: [:manage, :teams]
  end

  private

  def team_params
    params.require(:team).permit(:name)
  end
end
