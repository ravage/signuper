class Manage::BaseController < ApplicationController
  before_filter :authenticate_manager!

  layout 'manage'

end
