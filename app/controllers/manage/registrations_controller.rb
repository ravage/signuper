class Manage::RegistrationsController < Manage::BaseController
  respond_to :html

  def index
    @events = Event.joins(:registrations).
      includes(registrations: [:price,
        { user: :profile },
        :transactions,
        :participants,
        :team]).distinct.
      order('transactions.created_at, profiles.first_name, profiles.last_name')
  end

  def new
    @event = Event.find(params[:event_id])
    @users = User.includes(:profile).not_participating(@event.id)
  end

  def show
    @registration = Registration.find(params[:id])
    @transactions = Transaction.includes(:payment_proof).
      where(registration: @registration).order('id');
  end

  def create
    @event = Event.find(params[:event_id])

    unless params.key?(:registration) && params[:registration].key?(:user_ids)
      flash[:alert] = 'Não foi selecionado nenhum utilizador!'
      redirect_to :back and return
    end

    @users = User.not_participating(@event.id, params[:registration][:user_ids])
    
    @users.each { |user| IndividualRegistration.new(user, @event).save }

    redirect_to :back
  end

  def approve
    @registration = Registration.find(params[:id])
    @registration.update_attribute(:state, Registration::VERIFIED)

    redirect_to manage_registration_path(@registration)
  end

  def revoke
    @registration = Registration.find(params[:id])
    @registration.update_attribute(:state, Registration::REVOKED)

    redirect_to manage_registration_path(@registration)
  end
end
