class Manage::TransactionsController < Manage::BaseController
  respond_to :html

  def update
    @transaction = Transaction.find(params[:id])

    if params.key?(:revoke)
      params[:transaction][:amount] = 0
      params[:transaction].reverse_merge!(state: Transaction::REVOKED)
    else
      params[:transaction].reverse_merge!(state: Transaction::VERIFIED)
      params[:transaction].reverse_merge!(verified_at: Time.now)
    end
    
    if @transaction.update_attributes!(transaction_params)
      UserMailer.delay_for(30.seconds).transaction(@transaction.id)
    end

    respond_with @transaction, location: [:manage, @transaction.registration]
  end

  private

  def transaction_params
    params.require(:transaction).permit(:amount, :state, :verified_at)
  end
end
