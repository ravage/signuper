class Manage::UsersController < Manage::BaseController
  respond_to :html

  def index
    @users = User.joins('LEFT JOIN profiles ON profiles.user_id = users.id').
      includes(profile: :gender).
      order('profiles.first_name, profiles.last_name')
  end

  def new
    @user = User.new
    @user.build_profile
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    @user.password = Devise.friendly_token
    @user.skip_confirmation!

    if @user.save
      flash[:success] = 'Operação concluída com sucesso'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação'
    end

    respond_with @user, location: [:manage, :users]
  end

  private 

  def user_params
    params.require(:user).permit(
      :email,
      profile_attributes: [
        :first_name,
        :last_name,
        :birthdate,
        :gender_id,
        :postal_code,
        :postal_extension,
        :citizen_id,
        :country_id,
        :phone_number
      ]
    )
  end
end
