class Manage::EntriesController < Manage::BaseController
  respond_to :html

  def index
    @event = Event.includes(:entries).find(params[:event_id])
    @entries = @event.entries
  end

  def new
    @event = Event.find(params[:event_id])
    @entry = @event.entries.build
  end

  def create
    @event = Event.find(params[:event_id])
    @entry = @event.entries.build(entry_params)

    if @entry.save
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with(@entry, location: [:manage, @event, :entries])
  end

  def edit
    find_models
  end

  def update
    find_models

    if @entry.update_attributes(entry_params)
      flash[:success] = 'Operação concluída com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with(@entry, location: [:manage, @event, :entries])
  end

  def destroy
    find_models

    if @entry.destroy
      flash[:success] = 'Operação concluída com sucesso!'
    end

    respond_with(@entry, location: [:manage, @event, :entries])
  end

  private
  def entry_params
    params.require(:entry).permit(:title, :content, :publish)
  end

  def find_models
    @event = Event.find(params[:event_id])
    @entry = @event.entries.find(params[:id])
  end
end