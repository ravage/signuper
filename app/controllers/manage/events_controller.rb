class Manage::EventsController < Manage::BaseController
  respond_to :html

  def index
    @events = Event.includes(:participants)
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def show
    @event = Event.find(params[:id])

    @teams = Team.joins(:registrations).
      includes(user: :profile).
      where(registrations: { event: @event }).
      group('teams.id').order('teams.name')

    registrations = Registration.where(event: @event).
      includes(participants: { user: :profile })

    @participants = Hash.new { |hash, key| hash[key] = [] }
    
    @team_count = 0
    @individual_count = 0

    registrations.map do |registration|
      if (registration.team_id == Team::INDIVIDUAL)
        @individual_count += 1
      else
        @team_count += 1
      end

      @participants[registration.team_id].concat(registration.participants)
    end

  end

  def create
    @event = Event.new(event_params)

    if @event.save
      flash[:success] = 'Operação realizada com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with @event, location: new_manage_event_event_option_path(@event)
  end

  def update
    @event = Event.find(params[:id])

    if @event.update_attributes(event_params)
      flash[:success] = 'Operação realizada com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    if params.fetch(:event) { |key| {} }.key?(:assets)
      @image = EventImage.new
      @image.attachment = params[:event][:assets]
      @event.event_images << @image
    end

    if @image
      respond_with @event, location: [:edit, :manage, @event]
    else
      respond_with @event, location: [:manage, :events]
    end
  end

  private

  def event_params
    params.require(:event).permit(
      :name,
      :summary,
      :description,
      :address,
      :postal_code,
      :postal_extension,
      :longitude,
      :latitude,
      :begins_at,
      :ends_at,
      :price,
      :bank_account_number,
      :max_participants,
      :registration_limit
    )
  end
end
