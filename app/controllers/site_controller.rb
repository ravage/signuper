class SiteController < ApplicationController
  def index
    @events = Event.upcoming.order(:registration_limit)
  end
end
