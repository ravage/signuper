class Account::TransactionsController < Account::BaseController
  respond_to :html

  def index
    @transactions = Transaction.where(user: current_user)
    .includes(registration: :event).includes(:payment_proof)
  end

  def new
    @registration = current_user.registrations
      .includes(participants: { user: :profile })
      .includes(transactions: :payment_proof).find(params[:registration_id])
    @transaction = Transaction.new

    respond_with @transaction
  end

  def create
    @registration = current_user.registrations
    .includes(participants: { user: :profile })
    .includes(transactions: :payment_proof).find(params[:registration_id])

    if params.include?(:transaction)
      payment_proof = PaymentProof.new
      payment_proof.attachment = params[:transaction][:assets]

      if payment_proof.valid?
        @transaction = Transaction.create do |o|
          o.registration = @registration
          o.user = current_user
          o.payment_proof = payment_proof
        end

        ManagerMailer.delay_for(30.seconds).transaction(@registration)
      else
        flash[:alert] = payment_proof.errors.full_messages.first
      end
    else
      flash[:alert] = 'Antes de enviares o comprovativo deves anexá-lo!'
    end

    respond_with @transaction, location: [:new, :account, @registration, :transaction]
  end

  def destroy
    @transaction = current_user.transactions.where(state: Transaction::PENDING).
      find(params[:id])

    @registration = current_user.registrations.find(params[:registration_id])

    @transaction.destroy

    respond_with @transaction, location: [:new, :account, @registration, :transaction]
  end
end
