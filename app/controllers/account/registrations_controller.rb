class Account::RegistrationsController < Account::BaseController
  respond_to :html

  def index
    @registrations = Registration.joins(:participants).
      includes(:team, :event, :price, participants: { user: :profile }).
      includes(user: :profile).
      where('participants.user_id = ? OR registrations.user_id = ?',
            current_user.id, current_user.id).uniq
  end

  def create
    unless current_user.profile.try(:persisted?)
      redirect_to account_profile_path,
        alert: 'Para prosseguir com a inscrição deve conluir o seu perfil.' and return
    end

    if params.key?(:team_registration)
      register_team
    else
      register_individual
    end
  end

  def destroy
    @event_option = EventOption.find(params[:event_option_id])
    @participation = Participant.where(user: current_user, event_option: @event_option).take!

    @registration = Registration.where(
      user: current_user,
      event_option: @event_option,
      id: @participation.registration_id
    ).take
    
    if @registration
      if @registration.team?
        if @registration.participants.size > 1
          @registration.participants.where(user: current_user).destroy_all
        else
          @registration.participants.destroy_all
          @registration.transactions.destroy_all
          @registration.destroy
        end
      else
        @participation.destroy
        @registration.transactions.destroy_all
        @registration.destroy
      end
    else
      @participation.destroy
    end

    respond_with @registration, location: [:individual, :account, @event_option, :registrations]
  end

  def register_team
    @event_option = EventOption.find(params[:event_option_id])

    unless @event_option.available?
      redirect_to [:account, :dashboard] and return
    end

    session[:registration] = team_account_event_option_registrations_path
    @registrations = @event_option.registrations.by_user(current_user)

    @team_registration = TeamRegistration.new(
      @event_option, current_user, params[:team_registration])

    @teams = current_user.teams

    if @team_registration.save
      flash[:success] = 'Operação realizada com sucesso!'
    else
      if @team_registration.errors.include?(:registration_exists)
        flash[:alert] =  'O atleta que tentaste adicionar já se encontra inscrito neste evento.'
      elsif @team_registration.errors.include?(:account_exists)
        flash[:alert] = 'O atleta para o qual tentaste criar conta já existe no sistema. Experimenta adicioná-lo ao evento através da pesquisa de CC, opção #2.'
      elsif @team_registration.errors.include?(:citizen_id_search)
        flash[:alert] = 'Atleta não encontrado.'
      else
        flash[:alert] = 'Ocorreu um erro ao validar a sua operação!'
      end
    end

    session[:team] = @team_registration.team

    respond_with @team_registration, location: [:team, :account, @event_option, :registrations]
  end

  def register_individual
    @event_option = EventOption.find(params[:event_option_id])

    @registration = IndividualRegistration.new(current_user, @event_option)

    if @registration.save
      flash[:success] = 'Operação realizada com sucesso!'
      redirect_to [:account_registrations]
    else
      flash[:alert] = 'Ocorreu um erro ao validar a sua operação!'
      render 'individual'
    end
  end

  def individual
    @event_option = EventOption.find(params[:event_option_id])
    @event = @event_option.event
    session[:registration] = individual_account_event_option_registrations_path
    @registration = Registration.new
  end

  def team
    @event_option = EventOption.find(params[:event_option_id])
    @event = @event_option.event
    session[:registration] = team_account_event_option_registrations_path
    @team_registration = TeamRegistration.new(@event_option, current_user)
    @registrations = @event_option.registrations.by_user(current_user)
    @teams = current_user.teams
  end
end
