class Account::ParticipantsController < Account::BaseController
  respond_to :html

  def destroy
    @participant = Participant.select('participants.*').joins(:registration).
    where(registrations: { user_id: current_user.id }).find(params[:id])

    if @participant.registration.team_id == Team::INDIVIDUAL
      @participant.destroy
      @participant.registration.destroy
    else
      @participant.destroy
    end

    respond_with @participant, location: session[:registration]
  end
end
