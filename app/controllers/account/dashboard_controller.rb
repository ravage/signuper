class Account::DashboardController < Account::BaseController
  def index
    @events = Event.all#upcoming.order('registration_limit')
    @participations = current_user.participations #.where('events.begins_at > ?', Time.now)
    @registrations =  current_user.registrations.
      includes(:team, :event, :transactions, participants: { user: :profile }).
      where('team_id <> ?', 1).#.where('events.begins_at > ?', Time.now).
      references(:events)
  end
end
