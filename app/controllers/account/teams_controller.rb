class Account::TeamsController < Account::BaseController
  respond_to :html

  def index
    @teams = current_user.teams.includes(:registrations)
  end

  def new
    @team = Team.new
  end

  def edit
    @team = current_user.teams.find(params[:id])
  end

  def create
    @team = current_user.teams.new(team_params)

    if @team.save
      flash[:success] = 'Operação realizada com sucesso'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação'
    end
    
    respond_with @team, location: [:account, :teams]
  end

  def update
    @team = current_user.teams.find(params[:id])

    if @team.update_attributes(team_params)
      flash[:success] = 'Operação realizada com sucess'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação'
    end

    respond_with @team, location: [:account, :teams]
  end

  def destroy
    @team = current_user.teams.without_participations.find(params[:id])

    @team.destroy

    respond_with @team, location: [:account, :teams]
  end

  private

  def team_params
    params.require(:team).permit(:name)
  end
end
