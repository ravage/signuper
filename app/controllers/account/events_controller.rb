class Account::EventsController < Account::BaseController
  def checkout
    @event = Event.find(params[:id]) 
    @registrations = Registration.includes(user: :profile).where(registrator: current_user)
  end
end
