# encoding: utf-8
class Account::ProfilesController < Account::BaseController
  respond_to :html

  def show
    @profile = current_user.profile || Profile.new(
      first_name: session[:first_name], last_name: session[:last_name])
  end

  def create
    @profile = current_user.create_profile(profile_params)

    respond_with @profile, location: account_profile_path
  end

  def update
    @profile = current_user.profile

    if @profile.update_attributes(profile_params)
      flash[:success] = 'Operação realizada com sucesso!'
    else
      flash[:alert] = 'Ocorreu um erro ao realizar a operação!'
    end

    respond_with @profile, location: account_profile_path
  end

  private

  def profile_params
    params.require(:profile).permit(
      :first_name,
      :last_name,
      :birthdate,
      :gender_id,
      :postal_code,
      :postal_extension,
      :phone_number,
      :citizen_id,
      :country_id,
      :shirt_size
    )
  end
end
