class Account::ContactsController < Account::BaseController
  respond_to :html

  def show
    @message = Contact.new(from: current_user.email)
  end

  def create
    @message = Contact.new(params[:contact])

    if @message.save
      flash[:success] = 'Mensagem enviada com sucesso!'
    else
      flash[:alert] = 'Erro ao enviar mensagem. Por favor, verifica se todos os campos estão corretos!'
    end

    respond_with @message, location: [:account, @message]
  end
end
