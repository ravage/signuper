module ApplicationHelper
  def current_page_in?(*controller)
    controller.include?(params[:controller])
  end

  def title(page_title)
    content_for(:title, page_title.to_s)
  end

  def markdown(text)
    sanitize(Kramdown::Document.new(text, auto_ids: false).to_html,
      tags: %w{iframe img p h1 h2 h3 h4 h5 h6 ul li ol a br strong em},
      attributes: %w{width height frameborder allowfullscreen src href alt})
  end

  def attachment_cdn(attachment)
    host = if Rails.env.development?
      "#{request.host}:#{request.port}"
    else
      "cdn.#{request.host}"
    end

    '//' + host + attachment.url
  end
end
