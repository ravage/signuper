class NameInput < SimpleForm::Inputs::Base
  def input
    output = ''
    output << @builder.text_field(:first_name, input_html_options)
    output << @builder.text_field(:last_name, input_html_options)
    p @builder

    output.html_safe
  end
end
