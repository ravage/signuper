class Category < ActiveRecord::Base
  has_many :participants

  validates :name, presence: true
end
