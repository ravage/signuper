class EventOption < ActiveRecord::Base
  belongs_to :event
  has_many :registrations
  has_many :participants, through: :registrations
  has_many :prices

  validates :name, :event_id, :max_participants, :registration_limit, presence: true

  def available?
    registration_limit >= Time.zone.now.to_date &&
      max_participants > participants.size
  end

  def transactions_for(user)
    @transactions ||= Transaction.where(event: self, user: user)
  end

  def price
    @price ||= prices.current
  end

  def available?
    registration_limit >= Time.zone.now.to_date &&
      max_participants > participants.size
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
