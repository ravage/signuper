class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
    :recoverable, :trackable, :validatable, :confirmable, :async,
    :omniauthable, :omniauth_providers => [:facebook]

  has_one :profile, inverse_of: :user
  has_many :registrations, inverse_of: :user
  has_many :events, through: :registrations
  has_many :teams
  has_many :transactions
  has_many :auth_providers

  accepts_nested_attributes_for :profile

  def registered?(event)
    @events ||= Event.joins(:participants).where(participants: { user: self })
    @events.include?(event)
  end

  def manages?(event_option)
    @manages_event ||= Registration.where(user: self, event_option: event_option).
      where('team_id <> ?', Team::INDIVIDUAL).exists?
  end

  def registered_for_team?(event, team)
    @events_team ||= Event.joins(registrations: [:participants, :team]).
      where(participants: { user: self }, registrations: { team: team })
    @events_team.include?(event)
  end

  def participations
    Participant.where(user: self).includes(:event, registration: [
      :transactions, :team, :event, :participants, user: :profile]).
      references(:event)
  end

  def name
    try(:profile).try(:name) || email
  end

  def self.not_participating(event, users = nil)
    if users.nil?
      User.joins(:profile).
        where('users.id NOT IN (SELECT user_id FROM participants WHERE event_id = ?)', event)
    else
      User.find_by_sql([
        'SELECT users.* FROM users
        WHERE id NOT IN (SELECT user_id FROM participants WHERE event_id = ?)
        AND users.id IN (?)', event, users])
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource = nil)
    provider = AuthProvider.where(provider: auth.provider, uid: auth.uid).take

    return nil if provider.nil? && User.where(email: auth.info.email).take
    
    unless provider
      ActiveRecord::Base.transaction do
        user = User.create! do |o|
          o.email = auth.info.email
          o.password = Devise.friendly_token
          o.skip_confirmation!
        end

        provider = user.auth_providers.create! do |o|
          o.provider = auth.provider
          o.uid = auth.uid
        end
      end
    end

    provider.user
  end
end
