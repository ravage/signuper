class Price < ActiveRecord::Base
  belongs_to :event
  has_many :registrations

  validates :value, :date, presence: true
  validates :value, numericality: { greater_than: 0 }

  def self.current
    where('date >= ?', Time.zone.now.to_date).order(:date).take
  end

  def +(v)
    value + v
  end

  def *(v)
    value * v
  end
end
