class EventImage < Asset
  mount_uploader :attachment, EventImageUploader

  validates :attachment, presence: true
  validates_integrity_of :attachment
  validate :attachment_size_validation

  private

  def attachment_size_validation
    errors[:attachment] << 'deve ser inferior a 1MB' if attachment.size > 1.megabyte
  end
end