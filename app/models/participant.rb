class Participant < ActiveRecord::Base
  belongs_to :event
  belongs_to :event_option
  belongs_to :registration, inverse_of: :participants
  belongs_to :user
  belongs_to :category

  validates :event_id, uniqueness: { scope: :user_id }
  validates :identification, uniqueness: { scope: :event_id },
    if: proc { |a| a.identification.present? }
end
