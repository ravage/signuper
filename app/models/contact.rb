class Contact
  include ActiveModel::Model

  attr_accessor :from
  attr_accessor :subject
  attr_accessor :body

  validates :from, :subject, :body, presence: true
  validates :from, format: { with: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i,
                             message: 'Email inválido, exemplo: nome@exemplo.com' }

  def initialize(attrs = {})
    attrs.each do |attr, value|
      public_send("#{attr}=", value)
    end
  end

  def save
    persist! if valid?
  end
    
  def persisted?
    false
  end

  private

  def persist!
    UserMailer.delay.contact(self)        
  end
end
