class Transaction < ActiveRecord::Base
  VERIFIED = 'A'
  REVOKED = 'R'
  PENDING = 'P'

  scope :verified, -> { where(state: VERIFIED) }

  belongs_to :registration, inverse_of: :transactions
  belongs_to :user
  has_one :payment_proof, as: :attachable, dependent: :destroy

  def verified?
    state == VERIFIED
  end

  def revoked?
    state == REVOKED
  end

  def humanize_state
    case state
    when VERIFIED
      'Aprovado'
    when REVOKED
      'Revogado'
    when PENDING
      'Pendente'
    end
  end
end
