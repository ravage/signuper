class Gender < ActiveRecord::Base
  has_many :profiles

  def female?
    name == 'Feminino'
  end

  def male?
    name == 'Masculino'
  end

  def to_s
    name
  end
end
