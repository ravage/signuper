class Entry < ActiveRecord::Base
  PUBLISHED = 'P'
  UNPUBLISHED = 'U'
  
  scope :published, -> { where(state: PUBLISHED) }

  belongs_to :event

  validates :title, :content, presence: true

  attr_accessor :publish

  def published?
    state == PUBLISHED
  end

  def publish=(value)
    self.state = (value.to_i == 0) ? UNPUBLISHED : PUBLISHED
  end

  def publish
    (state == PUBLISHED) ? 1 : 0
  end
end
