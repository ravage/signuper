class Registration < ActiveRecord::Base
  VERIFIED = 'A'
  REVOKED = 'R'
  PENDING = 'P'

  belongs_to :user, inverse_of: :registrations
  belongs_to :team
  belongs_to :event, inverse_of: :registrations
  belongs_to :event_option
  belongs_to :price

  has_many :participants, inverse_of: :registration
  has_many :transactions, inverse_of: :registration

  validates :team_id, :user_id, :event_id, presence: true
  validates :event_id, uniqueness: { scope: [:user_id, :team_id] }

  def paid?
    state == VERIFIED
  end

  def team?
    team_id != Team::INDIVIDUAL
  end

  def managed_by?(user)
    self.user == user
  end

  def self.by_user(user)
    where(user: user).includes(participants: { user: :profile }).includes(:team)
  end

  def balance
    verified = transactions.inject(0) do |sum, transaction|
      amount = transaction.verified? ? transaction.amount : 0
      sum + amount
    end
    
    result = total - verified

    result < 0 ? 0 : result
  end

  def total
    cost = price || 0
    cost * participants.length
  end

  def paid
    transactions.inject(0) do |sum, transaction|
      amount = transaction.verified? ? transaction.amount : 0
      sum + amount
    end
  end
end
