class Event < ActiveRecord::Base
  has_many :participants
  has_many :registrations, inverse_of: :event
  has_many :participants, through: :registrations
  has_many :entries

  validates :name, :description, presence: true
  validates :registration_limit, presence: true

  has_many :event_images, as: :attachable
  has_many :options, class_name: 'EventOption'

  def transactions_for(user)
    @transactions ||= Transaction.where(event: self, user: user)
  end

  def self.upcoming(count = 10)
    Event.where('registration_limit >= ?', Time.zone.now)
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end
end
