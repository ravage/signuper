class TeamRegistration
  include ActiveModel::Model

  attr_reader :profile

  attr_accessor :team
  attr_accessor :team_select
  attr_accessor :team_input
  attr_accessor :email
  attr_accessor :citizen_id_search
  attr_accessor :citizen_id
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :birthdate
  attr_accessor :gender_id
  attr_accessor :postal_code
  attr_accessor :postal_extension
  attr_accessor :phone_number
  attr_accessor :country
  attr_accessor :shirt_size

  validates :team, presence: true
  validate :team_uniqueness

  validates :team_input, presence: true, if: Proc.new { |a| a.team_select.blank? }
  validates :team_select, presence: true, if: Proc.new { |a| a.team_input.blank? }

  validates :citizen_id, :first_name, :last_name, :birthdate, :gender_id,
    :postal_code, :postal_extension, :phone_number, :email, :country,
    presence: true, unless: Proc.new { |a| a.citizen_id_search.present? }

  validates :postal_code, :postal_extension,
    numericality: {message: 'Para Código ou Extensão Postal deve indicar apenas números'},
    unless: Proc.new { |a| a.citizen_id_search.present? }

  validates :email, format: { with: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i },
    unless: Proc.new { |a| a.citizen_id_search.present? }

  validate :profile_existence, if: Proc.new { |a| a.citizen_id_search.present? }
  validate :profile_uniqueness, if: Proc.new { |a| a.citizen_id.present?  }
  validate :athlete_uniqueness, if: Proc.new { |a| a.email.present? }
  validate :birthdate_format, unless: Proc.new { |a| a.citizen_id_search.present? }

  def initialize(event_option, registrator, params = {})
    @event_option = event_option
    @event = @event_option.event
    @registration = Registration.new
    @registrator = registrator

    params.each do |attr, value|
      public_send("#{attr}=", value)
    end

    @team = params[:team_select].blank? ? params[:team_input] : params[:team_select]
  end

  def save
    persist! if valid?
  end

  def persisted?
    false
  end

  def name_error
    [:first_name, :last_name].each do |attr|
      return errors.get(attr).first if errors.get(attr).present?
    end

    nil
  end

  def postal_error
    [:postal_code, :postal_extension].each do |attr|
      return errors.get(attr).first if errors.get(attr).present?
    end

    nil
  end

  private

  def persist!
    if @citizen_id_search.present?
      persist_from_search
    else
      persist_from_new
    end
  end

  def persist_from_search
    @profile ||= Profile.where(citizen_id: @citizen_id).take

    if @profile
      ActiveRecord::Base.transaction do
        team = @registrator.teams.find_or_create_by!(name: @team)

        begin
          registration = Registration.find_or_create_by!(
            event: @event,
            event_option: @event_option,
            user: @registrator,
            team: team) do |o|
            o.price = @event_option.price
          end

          participant = Participant.create! do |o|
            o.registration = registration
            o.event_option = @event_option
            o.event = @event
            o.user_id = @profile.user_id
          end

          notify_manager(registration)
          notify_user(registration, participant)
        rescue ActiveRecord::RecordInvalid
          errors.add(:registration_exists, 'Registo já existente')
          raise ActiveRecord::Rollback
        end
      end
    end

    errors.empty?
  end

  def persist_from_new
    registration = nil

    ActiveRecord::Base.transaction do
      team = @registrator.teams.find_or_create_by!(name: @team)

      user = User.create! do |o|
        o.skip_confirmation!
        o.email = @email
        o.password = Devise.friendly_token
      end

      Profile.create! do |o|
        o.first_name        = @first_name
        o.last_name         = @last_name
        o.birthdate         = @birthdate
        o.gender_id         = @gender_id
        o.postal_code       = @postal_code
        o.postal_extension  = @postal_extension
        o.phone_number      = @phone_number
        o.citizen_id        = @citizen_id
        o.country_id        = @country
        o.shirt_size        = @shirt_size
        o.user              = user
      end if user.profile.nil?

      begin
        registration = Registration.find_or_create_by!(
          event: @event,
          event_option: @event_option,
          user: @registrator,
          team: team) do |o|
          o.team  = team
          o.price = @event_option.price
        end

        participant = Participant.create! do |o|
          o.registration = registration
          o.event = @event
          o.event_option = @event_option
          o.user = user
        end

        notify_manager(registration)
        notify_user(registration, participant)
      rescue ActiveRecord::RecordInvalid
        errors.add(:registration_exists, 'Registo já existente')
        raise ActiveRecord::Rollback
      end
    end
    
    errors.empty?
  end

  def profile_existence
    @profile = Profile.where(citizen_id: @citizen_id_search).take
    errors.add(:citizen_id_search, 'Atleta não encontrado') unless @profile
  end

  def profile_uniqueness
    @profile = Profile.where(citizen_id: @citizen_id).take
    if @profile
      errors.add(:citizen_id, 'Já existe um atleta com o número de CC fornecido')
      errors.add(:account_exists, 'Registo já existe') if @user
    end
  end

  def athlete_uniqueness
    @user = User.where(email: @email).take
    errors.add(:email, 'Já existe um atleta com o email fornecido') if @user
    errors.add(:account_exists, 'Registo já existe') if @user
  end

  def team_uniqueness
    team = Team.where(name: @team).where.not(user: @registrator).take
    errors.add(:team_input, 'Já existe uma equipa com esse nome registada por outro utilizador!') if team
  end

  def birthdate_format
    begin
      @birthdate = Date.parse(@birthdate)
    rescue ArgumentError
      errors.add(:birthdate, 'Data de Nascimento possui formato inválido. Formato: dia/mês/ano')
    end
  end

  def notify_manager(registration)
    if registration.participants.size == 1
      ManagerMailer.delay_for(30.seconds).registration(registration.id)
      UserMailer.delay_for(30.seconds).registration(registration.id)
    end
  end
  
  def notify_user(registration, participant)
    UserMailer.delay_for(30.seconds).managed_registration(registration.id, participant.id)
  end
end
