class Asset < ActiveRecord::Base
  before_save :update_asset_attributes

  belongs_to :attachable, polymorphic: true

  def image?
    content_type.starts_with?('image') if content_type
  end

  def pdf?
    content_type.ends_with?('pdf') if content_type
  end

  def video?
    content_type.starts_with?('video') if content_type
  end

  def update_asset_attributes
    if attachment.present? && attachment_changed?
      self.content_type = attachment.file.content_type
    end
  end
end
