class IndividualRegistration
  include ActiveModel::Model

  attr_reader :registration
  attr_reader :participant

  def initialize(user, event_option)
    @user = user
    @event = event_option.event
    @event_option = event_option

    build
  end

  def save
    persist! if valid?
  end

  def persisted?
    false
  end

  private

  def build
    @registration = Registration.new.tap do |o|
      o.event = @event
      o.event_option = @event_option
      o.user = @user
      o.price = @event_option.price
    end

    @participant = Participant.new.tap do |o|
      o.registration = @registration
      o.event = @event
      o.event_option = @event_option
      o.user = @user
    end
  end

  def persist!
    ActiveRecord::Base.transaction do
      begin
        @registration.save!
        @participant.save!

        notify_manager(@registration)
      rescue ActiveRecord::RecordInvalid
        errors.add(:registration_exists, 'Registo já existente')
        raise ActiveRecord::Rollback
      end
    end

    errors.empty?
  end
  
  private 

  def notify_manager(registration)
    ManagerMailer.delay_for(30.seconds).registration(registration.id)
    UserMailer.delay_for(30.seconds).registration(registration.id)
  end
end
