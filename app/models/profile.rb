class Profile < ActiveRecord::Base
  SHIRT_SIZES = %w{ S M L XL XXL }
  belongs_to :user, inverse_of: :profile
  belongs_to :gender
  belongs_to :country

  validates :first_name, :last_name, :birthdate, :gender, :citizen_id, :country, presence: true
  validates :postal_code, numericality: true, presence: true
  validates :postal_extension, presence: true,
    numericality: { message: 'Extensão Postal é um valor numérico, a segunda parte do teu código postal' }
  validates :phone_number, presence: true
  validates :citizen_id, uniqueness: { message: I18n.t('profile.citizen_id.uniqueness') }

  def name
    "#{first_name} #{last_name}"
  end

  def name_error
    [:first_name, :last_name].each do |attr|
      return errors.get(attr).first if errors.get(attr).present?
    end

    nil
  end

  def postal_error
    [:postal_code, :postal_extension].each do |attr|
      return errors.get(attr).first if errors.get(attr).present?
    end

    nil
  end

  def age
    now = Time.now.utc
    now.year - birthdate.year - (birthdate.to_time.change(
      :year => now.year) > now ? 1 : 0)
  end
end
