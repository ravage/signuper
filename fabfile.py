from fabric.api import *

env.hosts = ['famarunners.com']
env.user = 'deployer'

deploy_to = '/srv/sites/famarunners.com'
repository = 'git@bitbucket.org:ravage/signuper.git'
environment = 'production'

def sync():
    """docstring for sync_repository"""
    local('git push')
    with cd(deploy_to):
        run('git pull')

def clone():
    """docstring for clone"""
    with cd(deploy_to):
        run('git clone %s .' % repository)

def bundle():
    """docstring for bundle"""
    with cd(deploy_to):
        run('bundle')


def copy_private_files():
    """docstring for copy_private_files"""
    with cd(deploy_to):
        put('db/flyway/conf/flyway.properties.production', 'db/flyway/conf/flyway.properties', mode=0600)
        put('config/database.yml', 'config/database.yml', mode=0600)
        put('config/config_production.yml', 'config/config_production.yml', mode=0600)

def db_setup():
    """docstring for rails_migrate"""
    with cd(deploy_to):
        with shell_env(RAILS_ENV=environment):
            run('rake db:migrate')
            run('db/flyway/flyway.sh migrate')

def bower():
    """docstring for bower"""
    with cd(deploy_to):
        run('bower install')

def assets_precompile():
    """docstring for assets_precompile"""
    with cd(deploy_to):
        with shell_env(RAILS_ENV=environment):
            run('rake assets:precompile')

def tmp_folders():
    """docstring for tmp_folders"""
    with cd('%s/tmp' % deploy_to):
        with settings(warn_only=True):
            run('mkdir cache pids sessions sockets')

def sidekiq():
    """docstring for sidekiq"""
    with cd(deploy_to):
        with settings(warn_only=True):
            run('bundle exec sidekiqctl quiet tmp/pids/sidekiq.pid')
            run('bundle exec sidekiqctl stop tmp/pids/sidekiq.pid')
            run('exec bin/sidekiq -e production -d -C config/sidekiq.yml', pty=False)

def unicorn_stop():
    """docstring for start_unicorn"""
    run('sudo /etc/init.d/unicorn.signuper stop')

def unicorn_upgrade():
    """docstring for upgrade_unicord"""
    run('sudo /etc/init.d/unicorn.signuper upgrade')

def upgrade():
    """docstring for upgrade"""
    sync()
    assets_precompile()
    unicorn_upgrade()
    sidekiq()
