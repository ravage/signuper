RAILS_ROOT=/srv/sites/famarunners.com

pid=`cat $RAILS_ROOT/unicorn.pid`
kill -QUIT $pid
sleep 3
sudo nginx
unicorn -D -d -E production -c $SCRIPT_HOME/unicorn.rb
