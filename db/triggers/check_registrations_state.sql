CREATE OR REPLACE FUNCTION check_transactions() RETURNS TRIGGER AS $check_transactions$
  DECLARE
    paid integer;
    event_price numeric(12, 4);
    price_id integer;
    event_id integer;
    amount_paid numeric(12, 4);
    amount_to_pay numeric(12, 4);
    approved varchar(1) default 'A';
    pending varchar(1) default 'P'; 
    registration_state varchar(1);
    _registration_id bigint;
  BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
      _registration_id = NEW.registration_id;
    ELSE
      _registration_id = OLD.registration_id;
    END IF;

    -- When transactions are updated check if amount is fulfilled
    SELECT r.price_id, r.event_id INTO price_id, event_id FROM registrations r
    WHERE r.id = _registration_id;

    IF price_id IS NULL THEN
      SELECT value INTO event_price FROM prices p
      JOIN event_options eo ON eo.id = p.event_option_id
      WHERE date >= CURRENT_DATE;
    ELSE
      SELECT prices.value INTO event_price FROM prices
      WHERE prices.id = price_id;
    END IF;

    -- total paid for this registration
    SELECT SUM(amount) INTO amount_paid FROM transactions
    WHERE transactions.registration_id = _registration_id;
    
    -- how much to pay for all registered participants
    SELECT COUNT(*) * event_price INTO amount_to_pay FROM participants
    WHERE participants.registration_id = _registration_id;

    IF amount_paid >= amount_to_pay THEN
      registration_state := approved;
    ELSE
      registration_state := pending;
    END IF;

    UPDATE registrations SET state = registration_state
    WHERE id = _registration_id;

    -- RAISE LOG 'price_id % - event_id % - event_price %', price_id, event_id, event_price;
    -- RAISE LOG 'amount_paid % - amount_to_pay %', amount_paid, amount_to_pay;
    RETURN NULL;
  END;
$check_transactions$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_registration_state ON transactions;
DROP TRIGGER IF EXISTS update_registration_state ON participants;

CREATE TRIGGER update_registration_state
  AFTER UPDATE OF state ON transactions
  FOR EACH ROW
  EXECUTE PROCEDURE check_transactions();

CREATE TRIGGER update_registration_state
  AFTER INSERT OR DELETE ON participants
  FOR EACH ROW
  EXECUTE PROCEDURE check_transactions();
