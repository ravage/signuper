ALTER TABLE registrations
    ADD COLUMN event_option_id BIGINT;

ALTER TABLE participants
    ADD COLUMN event_option_id BIGINT;

ALTER TABLE registrations
  ADD CONSTRAINT fk_registrations_event_options FOREIGN KEY (event_option_id) REFERENCES event_options (id);

ALTER TABLE participants
  ADD CONSTRAINT fk_participants_event_options FOREIGN KEY (event_option_id) REFERENCES event_options (id);