CREATE TABLE prices (
  id SERIAL PRIMARY KEY,
  value NUMERIC(12, 4) DEFAULT 0,
  "date" DATE NOT NULL,
  event_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

ALTER TABLE prices
  ADD CONSTRAINT fk_prices_event FOREIGN KEY (event_id) REFERENCES events (id);

CREATE INDEX idx_prices_event_id ON prices (event_id);
