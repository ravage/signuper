CREATE TABLE entries (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  content TEXT NOT NULL,
  state VARCHAR(1) DEFAULT 'U',
  event_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

ALTER TABLE entries
  ADD CONSTRAINT fk_entry_event FOREIGN KEY (event_id) REFERENCES events (id);

CREATE INDEX idx_entry_event_id ON entries (event_id);
