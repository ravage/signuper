CREATE TABLE managers (
  id SERIAL PRIMARY KEY NOT NULL,
  email VARCHAR(128) NOT NULL,
  encrypted_password VARCHAR(255) NOT NULL,
  reset_password_token VARCHAR(255),
  reset_password_sent_at TIMESTAMP,
  remember_created_at TIMESTAMP,
  sign_in_count INT DEFAULT 0,
  current_sign_in_at TIMESTAMP,
  last_sign_in_at TIMESTAMP,
  current_sign_in_ip INET,
  last_sign_in_ip INET,
  unlock_token VARCHAR(255),
  unconfirmed_email VARCHAR(128),
  confirmation_token VARCHAR(255),
  confirmed_at TIMESTAMP,
  confirmation_sent_at TIMESTAMP,
  created_at TIMESTAMP NOT NULL,
  unlocked_at TIMESTAMP,
  updated_at TIMESTAMP,
  locked_at TIMESTAMP
);

CREATE UNIQUE INDEX idx_manager_email ON managers (email);
CREATE UNIQUE INDEX idx_manager_reset_token ON managers (reset_password_token);
