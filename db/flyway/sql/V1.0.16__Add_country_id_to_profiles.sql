ALTER TABLE profiles
  ADD COLUMN country_id VARCHAR(2),
  ADD CONSTRAINT fk_profiles_countries FOREIGN KEY (country_id) REFERENCES countries (id);

  CREATE INDEX idx_profiles_country_id ON profiles (country_id);
