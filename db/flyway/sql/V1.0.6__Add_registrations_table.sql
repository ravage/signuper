CREATE TABLE registrations (
  id BIGSERIAL PRIMARY KEY,
  event_id INT NOT NULL,
  user_id INT NOT NULL,
  team_id INT DEFAULT 1,
  state VARCHAR(1) DEFAULT 'P',
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

ALTER TABLE registrations
  ADD CONSTRAINT u_registration UNIQUE (event_id, user_id, team_id),
  ADD CONSTRAINT fk_registrations_event_id FOREIGN KEY (event_id) REFERENCES events (id),
  ADD CONSTRAINT fk_registrations_user_id FOREIGN KEY (user_id) REFERENCES users (id),
  ADD CONSTRAINT fk_registrations_team_id FOREIGN KEY (team_id) REFERENCES teams (id);

CREATE INDEX idx_registrations_event_id ON registrations (event_id);
CREATE INDEX idx_registrations_user_id ON registrations (user_id);
CREATE INDEX idx_registrations_team_id ON registrations (team_id);
