CREATE TABLE events (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  summary VARCHAR(1024),
  description TEXT NOT NULL,
  address VARCHAR(255),
  postal_code INT,
  postal_extension INT,
  longitude NUMERIC(9, 6),
  latitude NUMERIC(9, 6),
  price NUMERIC(12, 4) DEFAULT 0,
  begins_at TIMESTAMP NOT NULL,
  ends_at TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);
