ALTER TABLE events
    DROP COLUMN begins_at,
    DROP COLUMN ends_at,
    DROP COLUMN max_participants;