ALTER TABLE registrations
  ADD COLUMN price_id INT,
  ADD CONSTRAINT fk_registrations_price_id FOREIGN KEY (price_id) REFERENCES prices (id);

CREATE INDEX idx_registrations_price_id ON registrations (price_id);
