CREATE TABLE event_options (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  event_id SERIAL NOT NULL,
  max_participants INT DEFAULT 0,
  registration_limit DATE DEFAULT CURRENT_DATE,
  price NUMERIC(12, 4) DEFAULT 0,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

ALTER TABLE event_options
  ADD CONSTRAINT fk_event_options_event FOREIGN KEY (event_id) REFERENCES events (id);

CREATE INDEX idx_event_options_event_id ON event_options (event_id);
