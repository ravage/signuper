CREATE TABLE assets (
  id SERIAL PRIMARY KEY,
  attachment VARCHAR(255) NOT NULL,
  attachable_id INT NOT NULL,
  attachable_type VARCHAR(255) NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE INDEX idx_assets_attachable_id ON assets (attachable_id);
CREATE INDEX idx_assets_attachable_type ON assets (attachable_type);
