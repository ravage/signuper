ALTER TABLE events
  ADD COLUMN max_participants INT DEFAULT 0,
  ADD COLUMN registration_limit DATE DEFAULT CURRENT_DATE;
