ALTER TABLE participants
  ADD COLUMN category_id INT,
  ADD COLUMN identification INT,
  ADD CONSTRAINT fk_participants_category FOREIGN KEY (category_id) REFERENCES categories (id);


CREATE INDEX idx_participants_category_id ON participants (category_id);
