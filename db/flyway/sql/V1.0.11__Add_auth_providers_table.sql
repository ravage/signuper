CREATE TABLE auth_providers (
  id SERIAL PRIMARY KEY,
  provider VARCHAR(128) NOT NULL,
  "uid" VARCHAR(255) NOT NULL,
  user_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

ALTER TABLE auth_providers
  ADD CONSTRAINT fk_auth_providers_user FOREIGN KEY (user_id) REFERENCES users (id);

CREATE UNIQUE INDEX idx_auth_providers_uid ON auth_providers(uid);
CREATE UNIQUE INDEX idx_auth_providers_uid_user_id ON auth_providers(uid, user_id);
CREATE INDEX idx_auth_providers_user_id ON auth_providers(user_id);

