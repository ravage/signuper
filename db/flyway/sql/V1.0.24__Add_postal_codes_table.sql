CREATE TABLE postal_codes (
    id SERIAL PRIMARY KEY,
    district_id INT NOT NULL,
    county_id INT NOT NULL,
    locality_id INT NOT NULL,
    locality VARCHAR(255) NOT NULL,
    postal_name VARCHAR(255),
    postal_code INT NOT NULL,
    postal_extension INT NOT NULL
);

CREATE INDEX idx_postal_code ON postal_codes(postal_code);
CREATE INDEX idx_postal_extension ON postal_codes(postal_extension);