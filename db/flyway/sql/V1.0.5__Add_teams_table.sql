CREATE TABLE teams (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  user_id INT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

ALTER TABLE teams
  ADD CONSTRAINT u_team_name UNIQUE (name),
  ADD CONSTRAINT fk_team_user FOREIGN KEY (user_id) REFERENCES users (id);

CREATE INDEX idx_teams_user_id ON teams (user_id);
