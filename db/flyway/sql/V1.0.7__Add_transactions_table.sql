CREATE TABLE transactions (
  id SERIAL PRIMARY KEY,
  registration_id BIGINT NOT NULL,
  user_id INT NOT NULL,
  amount NUMERIC(12, 4) DEFAULT 0,
  state VARCHAR(1) DEFAULT 'P',
  verified_at TIMESTAMP,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

ALTER TABLE transactions
  ADD CONSTRAINT fk_transactions_registration_id FOREIGN KEY (registration_id) REFERENCES registrations (id),
  ADD CONSTRAINT fk_transactions_user_id FOREIGN KEY (user_id) REFERENCES users (id);

CREATE INDEX idx_transactions_registration_id ON transactions (registration_id);
CREATE INDEX idx_transactions_user_id ON transactions (user_id);
