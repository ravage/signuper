CREATE TABLE profiles (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL,
  birthdate DATE NOT NULL,
  gender_id INT NOT NULL,
  user_id INT NOT NULL,
  postal_code INT NOT NULL,
  postal_extension INT,
  phone_number VARCHAR(128),
  citizen_id VARCHAR(128),
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

ALTER TABLE profiles
  ADD CONSTRAINT fk_profile_gender FOREIGN KEY (gender_id) REFERENCES genders (id),
  ADD CONSTRAINT fk_profile_user FOREIGN KEY (user_id) REFERENCES users (id);
  
CREATE INDEX idx_profiles_gender_id ON profiles (gender_id);
CREATE UNIQUE INDEX idx_profiles_user_id ON profiles (user_id);