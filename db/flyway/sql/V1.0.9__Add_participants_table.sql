CREATE TABLE participants (
  id BIGSERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  registration_id BIGINT NOT NULL,
  event_id INT NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

ALTER TABLE participants
  ADD CONSTRAINT u_participant UNIQUE (event_id, user_id),
  ADD CONSTRAINT fk_participants_event_id FOREIGN KEY (event_id) REFERENCES events (id),
  ADD CONSTRAINT fk_participants_user_id FOREIGN KEY (user_id) REFERENCES users (id),
  ADD CONSTRAINT fk_participants_registration_id FOREIGN KEY (registration_id) REFERENCES registrations (id);

CREATE INDEX idx_participants_event_id ON participants (event_id);
CREATE INDEX idx_participants_user_id ON participants (user_id);
CREATE INDEX idx_participants_registrations_id ON participants (registration_id);
